import { NgModule } from '@angular/core';

import { InputTextModule } from 'primeng/inputtext';
import { ToastModule } from 'primeng/toast';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipModule } from 'primeng/chip';
import { DropdownModule } from 'primeng/dropdown';
import { SkeletonModule } from 'primeng/skeleton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { MessageService } from 'primeng/api';




@NgModule({
  exports: [
    InputTextModule,
    InputTextareaModule,
    ButtonModule,
    SplitButtonModule,
    CheckboxModule,
    ChipModule,
    DropdownModule,
    SkeletonModule,
    ToastModule
  ],
  providers: [
    MessageService
  ]
})
export class PrimeComponentsModule { }
