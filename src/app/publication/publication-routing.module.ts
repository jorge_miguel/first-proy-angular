import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewPublicationComponent } from './pages/new-publication/new-publication.component';

const routes: Routes = [
  {
    path: 'new-publication',
    component: NewPublicationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicationRoutingModule { }
