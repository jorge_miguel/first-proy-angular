import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicationRoutingModule } from './publication-routing.module';
import { NewPublicationComponent } from './pages/new-publication/new-publication.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PrimeComponentsModule } from '../prime-components/prime-components.module';


@NgModule({
  declarations: [
    NewPublicationComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PrimeComponentsModule,
    PublicationRoutingModule
  ]
})
export class PublicationModule { }
