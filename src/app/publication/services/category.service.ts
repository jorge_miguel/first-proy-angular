import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Categoria, CategoriesResponse } from '../types/category.type';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private serverUrl: string = environment.server_url;

  constructor(private http: HttpClient) { }

    public getAllCategories(): Observable<Categoria[]>{

      return this.http.get<Categoria[]>(`${this.serverUrl}/categorias`);

    }

}
