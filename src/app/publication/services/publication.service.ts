import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { CreatePublication, Libro, LibroCreado } from '../types/publication.type';

@Injectable({
  providedIn: 'root'
})
export class PublicationService {

  private serverUrl: string = environment.server_url;

  constructor(private http: HttpClient) { }

  get accessToken(){
    return localStorage.getItem('accessToken') as string;
  }

  public createPublication(createPublication: CreatePublication){
    return this.http.post<LibroCreado>(
      `${this.serverUrl}/repository`,
      createPublication
      // {headers: { 'Authorization': 'token' } }
    );

  }

  public getPublicationById(id: number){
    return this.http.get<Libro>(`${this.serverUrl}/repository/${id}`);
  }

  public updatePDFPublicationById(id: number, formData: FormData){
    return this.http.put(
      `${this.serverUrl}/repository/${id}`,
      formData
      // { headers: {'Authorization': this.accessToken} }
    );
  }

  public updateImagePublicationById(id: number, formData: FormData){
    return this.http.put(
      `${this.serverUrl}/repository/${id}`,
      formData
      // { headers: {'Authorization': this.accessToken} }
    );
  }

}

