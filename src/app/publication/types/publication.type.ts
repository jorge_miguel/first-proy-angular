import { RegisterResponse } from "src/app/auth/types/auth.types";
import { Categoria } from "./category.type";

export type CreatePublication = {
  titulo: string;
  resumen: string;
  gestion_de_publicacion: number;
  categoria_id: number;
  usuario_id: number;
}
export type PublicationsResponse = {
  libros: Libro[];
}

export type PublicationResponse = {
  libro: Libro;
}

export type Libro = {
  id: number;
  titulo: string;
  resumen: string;
  gestion_de_publicacion: number;
  imagen_url_libro: string | null;
  public_id_imagen: string | null;
  doc_url_libro: string | null;
  doc_url_descarga: string | null;
  mime_type_doc: string | null;
  id_drive_archivo: string | null;
  usuario_id: number;
  categoria_id: number;
  usuario: RegisterResponse;
  categoria: Categoria;
  es_favorito_de: any[];

}

export type LibroCreado = {
  id: number;
  titulo: string;
  resumen: string;
  gestion_de_publicacion: number;
  usuario_id: number;
  categoria_id: number;
}

export type CreatePublicationResponse = {
  libro: LibroCreado;
  msg: string;
}

