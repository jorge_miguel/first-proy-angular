export type CategoriesResponse = {
  categorias: Categoria[];
}

export type Categoria = {
  id:        number;
  categoria: string;
}
