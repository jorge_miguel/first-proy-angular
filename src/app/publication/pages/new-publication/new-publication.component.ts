import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, TitleStrategy } from '@angular/router';
import { MessageService } from 'primeng/api';
import { CategoryService } from '../../services/category.service';
import { Categoria } from '../../types/category.type';
import { CreatePublication, LibroCreado } from '../../types/publication.type';
import { AuthService } from 'src/app/auth/service/auth.service';
import { PublicationService } from '../../services/publication.service';

@Component({
  selector: 'app-new-publication',
  templateUrl: './new-publication.component.html',
  styles: [
  ]
})
export class NewPublicationComponent {

  public isLoading: boolean = false;
  public imagen!: File | null;
  public pdf!: File | null;
  public categories: Categoria[] = [];
  public actualYear: number = new Date().getFullYear();
  public imageError: boolean = false;
  public pdfError: boolean = false;

  public formPublication: FormGroup = this.formBuilder.group({
    'titulo': ['', [Validators.required, Validators.minLength(8)]],
    'resumen': [, [Validators.required]],
    'gestion': [, [Validators.required, Validators.min(1000), Validators.max(this.actualYear)]],
    'categoria': [,Validators.required],
    'imagen': [, Validators.required],
    'pdf': [, Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private categoryService: CategoryService,
    private publicationService: PublicationService,
    private authService: AuthService
  ){}

  ngOnInit(): void{

    this.getAllCategories();

  }

  public getAllCategories(): void {
    this.categoryService.getAllCategories().subscribe({
      next: (response: Categoria[]) => {
        console.log(response);
        console.log(this.categories);
        this.categories = response;
        console.log(this.categories);
      },
      error: (error: any) => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: error });
      }
    });
  }


  createPublication(): void{
    this.isLoading = true;

    const data: CreatePublication = {
      titulo: this.formPublication.value.titulo,
      resumen: this.formPublication.value.resumen,
      gestion_de_publicacion: Number(this.formPublication.value.gestion),
      categoria_id: Number(this.formPublication.value.categoria),
      usuario_id: Number(this.authService.userAuth?.id)
    };

    this.publicationService.createPublication(data).subscribe({
      next: (response: LibroCreado) =>{
        const publicationId = response.id;
        if(this.imagen){
          const formData = new FormData();
          formData.append('archivo', this.imagen);
          this.updateImage(publicationId, formData);
        }
        if(this.pdf){
          const formData = new FormData();
          formData.append('archivo', this.pdf);
          this.updatePDF(publicationId, formData);
        }
        this.router.navigateByUrl("/")
      },
      error: (error: any) => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: error });
      }
    });

  }

  public updateImage(id: number, formData: FormData): void{

    this.publicationService.updateImagePublicationById(id, formData).subscribe();
  }

  public updatePDF(id: number, formData: FormData): void{

    this.publicationService.updatePDFPublicationById(id, formData).subscribe({
      next: () => this.router.navigateByUrl('/')
    });
  }


  public changeInputImage(event: Event): void{
    const eventInput = event.target as HTMLInputElement;
    if(eventInput.files){
      const fileSubmitted = eventInput.files[0];

      if(fileSubmitted && fileSubmitted.size < (3*1024*1024) && (fileSubmitted.type.includes('image/'))){
        this.imagen = fileSubmitted;
        this.imageError = false;
    }else{
        this.imagen = null;
        eventInput.value= '';
        this.imageError = true;
        this.formPublication.patchValue({ imagen: null });

      }

    }

  }

  public changeInputPDF(event: Event): void{

    const eventInput = event.target as HTMLInputElement;
    if(eventInput.files){
      const fileSubmitted = eventInput.files[0];

      if(fileSubmitted && fileSubmitted.size < (10*1024*1024) && (fileSubmitted.type === 'application/pdf' )){
        this.pdf = fileSubmitted;
        this.pdfError = false;
    }else{
        this.pdf = null;
        eventInput.value= '';
        this.pdfError = true;
        this.formPublication.patchValue({ pdf: null });

      }

    }

  }

}
