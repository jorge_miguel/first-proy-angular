import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent {

  public isLoading: boolean = false;

  public formLogin: FormGroup = this.formBuilder.group({
    'email': ['as@a.com', [Validators.required, Validators.email]],
    'password': ['12345@As', Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private messageService: MessageService
    ) {}

    public loginUser(): void {
      this.isLoading = true;
      this.authService.loginUser(this.formLogin.value).subscribe({
        next: (authenticated) => {
          if (authenticated) {
            //Mostrar un mensaje de exito, redirigir al home.
            this.router.navigateByUrl("/");
            this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Inicio exitoso.' });
          } else {
            //Mostrar un mensaje de error
            this.isLoading = false;
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Credenciales no correctas.' });
          }
        },
        error: (error: any) => {
          //Mostrar un mensaje de error
          this.isLoading = false;
          this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Hubo un error al iniciar sesión.' });
        }
      });
    }

}
