import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { strongPassword } from '../../validators/strong-password.validator';
import { AuthService } from '../../service/auth.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [
  ]
})
export class RegisterComponent {

  public isLoading: boolean = false;

  public formRegister: FormGroup = this.formBuilder.group({
    'nombres': ['Nombre de prueba', [Validators.required, Validators.minLength(5)]],
    'email': ['', [Validators.required, Validators.email]],
    'password': [, [Validators.required, strongPassword()]],
    'terminos': [false, Validators.requiredTrue]
  });

  /*Antes*/

  // public formBuilder!: FormBuilder;

  // constructor(formBuilder: FormBuilder){
  //   this.formBuilder = formBuilder;
  // }

  /*Ahora*/
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private messageService: MessageService
    ) {}

    public registerUser(): void {
      this.isLoading = true;
      const token = this.generateRandomToken();

      const registerData = {
        ...this.formRegister.value,
        token,
        estado: true };

      this.authService.registerUser(registerData).subscribe({
        next: (response) => {
          //Mostrar un mensaje de exito, redirigir al login, redirigir al home.
          this.router.navigateByUrl("/");

          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Registro exitoso.' });
        },
        error: (error: any) => {
          //mostrar el error
          this.isLoading = false;
          this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Algo salio mal con el registro.' });
        }
      });
    }
    private generateRandomToken(): string {
      const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      const tokenLength = 32;
      let token = '';
      for (let i = 0; i < tokenLength; i++) {
        token += characters.charAt(Math.floor(Math.random() * characters.length));
      }
      return token;
    }
}
