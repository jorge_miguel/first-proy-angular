export type RegisterResponse = {
  id:       number;
  nombres:  string;
  email:    string;
  password: string;
  terminos: boolean;
  token:    string;
  estado:   boolean;
}
