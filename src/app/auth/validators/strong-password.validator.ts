import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function strongPassword(): ValidatorFn{
  return (control: AbstractControl) : ValidationErrors | null => {
    const value = control.value;

    if (!value) {
      return null;
    }
    const isStrongPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,}$/.test(value);

    return !isStrongPassword ? {strongPassword: 'La contraseña debe tener como minimo 8 caracteres (Mayusculas, minisculos y un caracter especial)'}: null;
  }
}
