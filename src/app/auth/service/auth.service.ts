import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { RegisterResponse } from '../types/auth.types';
import { Observable, of } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private serverUrl: string = environment.server_url;
  private user!: RegisterResponse | null;
  public isAuth: boolean = false;
  private loggedInUser!: RegisterResponse;

  constructor(private http: HttpClient, private router: Router) {}

  get userAuth(){
    return this.user;
  }

  get accessToken(){
    return localStorage.getItem('token') as string;
  }

  get accessUser(){
    return JSON.parse(localStorage.getItem('accessUser') as string) as RegisterResponse;
  }

  get loggedInUserDetails(): RegisterResponse | undefined {
    return this.loggedInUser;
  }

  public registerUser(registerUser: RegisterResponse): Observable<RegisterResponse> {
    return this.http.post<RegisterResponse>(`${this.serverUrl}/usuarios`, registerUser).pipe(
      tap(response =>{
        this.user = response;
        this.isAuth = true;
        localStorage.setItem('accessUser', JSON.stringify(response));
        localStorage.setItem('token', response.password);
      })
    );
  }

  public loginUser(loginUser: RegisterResponse) {
    return this.http.post<RegisterResponse>(`${this.serverUrl}/usuarios`, loginUser).pipe(
      tap(response =>{
        this.user = response;
        this.isAuth = true;
        localStorage.setItem('accessUser', JSON.stringify(response));
        localStorage.setItem('token', response.token);
      })
    );
  }

  // public validateSession(): Observable<boolean>{
  //   return this.http.get(
  //     `${this.serverUrl}/usuarios`,
  //     { headers: { 'Authorization': this.accessToken } }
  //   ).pipe(
  //     map(response =>{
  //       this.user = this.accessUser;
  //       this.isAuth = true;
  //       return true;
  //     }),
  //     catchError(error => {
  //       return of(false);
  //     })
  //   );
  // }

  // public validateSessionPromise(){
  //   return new Promise<boolean>((resolve, reject) =>{
  //     this.validateSession().subscribe({
  //       next: resolve,
  //       error: reject
  //     });
  //   });
  // }

  public logout(): void{
    localStorage.clear();
    this.isAuth = false;
    this.user = null;
    this.router.navigateByUrl('/login');
  }

}
