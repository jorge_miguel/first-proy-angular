import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthUserResponse, Usuario, loginUser } from '../types/auth.types';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private serverUrl: string = environment.server_url;
  private currentUser: AuthUserResponse | null = null;
  public isAuth: boolean = false;

  constructor(private http: HttpClient) { }

  get currentUserAuth(): AuthUserResponse | null {
    return this.currentUser;
  }

  public loginUser(loginData: loginUser): Observable<AuthUserResponse> {
    return this.http.get<Usuario[]>(`${this.serverUrl}/usuarios`).pipe(
      tap((users: Usuario[]) => {
        const foundUser = users.find(user => user.email === loginData.email && user.password === loginData.password);
        if (foundUser) {
          // Construir la respuesta de autenticación
          const authResponse: AuthUserResponse = {
            usuario: foundUser,
            token: 'TOKEN_GENERADO', // Generar un token de autenticación aquí
            msg: 'Inicio de sesión exitoso'
          };
          // Guardar el usuario actual y el token en el almacenamiento local
          this.currentUser = authResponse;
          this.isAuth = true;
          localStorage.setItem('currentUser', JSON.stringify(authResponse));
        } else {
          throw new Error('Credenciales no válidas');
        }
      })
    );
  }

  public logout(): void {
    this.currentUser = null;
    this.isAuth = false;
    localStorage.removeItem('currentUser');
  }
}
