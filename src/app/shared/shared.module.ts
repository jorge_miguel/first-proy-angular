import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingListComponent } from './components/loading-list/loading-list.component';
import { PrimeComponentsModule } from '../prime-components/prime-components.module';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    LoadingListComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    PrimeComponentsModule,
    RouterModule
  ],
  exports: [
    LoadingListComponent,
    NotFoundComponent
  ]
})
export class SharedModule { }
