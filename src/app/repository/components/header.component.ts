import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/auth/service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: [`
  .active{
    text-decoration: underline;
    color: var(--blue-500) !important;
  }
  `]
})
export class HeaderComponent {

  public items: MenuItem[] = [];

  constructor(
    public authService: AuthService
  ) {}

  ngOnInit(): void{
    // this.authService.validateSession().subscribe();
    this.items = [
      { label: 'Perfil', icon:'pi pi-user', routerLink: 'profile'},
      { label: 'Tus publicaciones', icon:'pi pi-book', routerLink: 'your-publications'},
      { label: 'Nueva publicación', icon:'pi pi-plus', routerLink: 'new-publication'},
      { label: 'Cerrar sesión', icon:'pi pi-times', command: () => this.authService.logout()}
    ]
  }

}
