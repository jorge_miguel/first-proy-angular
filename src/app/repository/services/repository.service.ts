import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Libro, PublicationResponse } from 'src/app/publication/types/publication.type';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  private serverUrl: string = environment.server_url;

  constructor(private http: HttpClient) { }

  public getAllPublications(){
    // return this.http.get(`${this.serverUrl}/libros/${limit}`);
    // return this.http.get(`${this.serverUrl}/libros`);
    return this.http.get<Libro[]>(`${this.serverUrl}/repository`);
    // return this.http.get<Libro[]>(`${this.serverUrl}/repository?limit=${limit}`);
  }
}
