import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RepositoryComponent } from './repository.component';
import { HomeComponent } from './page/home.component';
import { PublicationsComponent } from './page/publications.component';
import { PublicationComponent } from './page/publication.component';
// import { isAuthGuard } from '../publication/guards/is-auth.guard';

const routes: Routes = [
  {
    path: '',
    component: RepositoryComponent,
    children:[
      {path: '', component: HomeComponent },
      {path: 'publications', component: PublicationsComponent },
      {path: 'publication/:id', component: PublicationComponent },
      // {path: '', canActivate: [isAuthGuard], loadChildren: () => import('../publication/publication.module').then(m => m.PublicationModule)}
      {path: '', loadChildren: () => import('../publication/publication.module').then(m => m.PublicationModule)}

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepositoryRoutingModule { }
