import { Component } from '@angular/core';
import { RepositoryService } from '../services/repository.service';
import { MessageService } from 'primeng/api';
import { response } from 'express';
import { ActivatedRoute } from '@angular/router';
import { Libro } from 'src/app/publication/types/publication.type';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent {

  public isLoadingPage: boolean = true;
  public publications: Libro[] = [];
  // public limit: number = 2;

  constructor(
    private route: ActivatedRoute,
    private repositoryService: RepositoryService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      // this.limit = +params['paginacion'] || 2;
      this.getLatestPublications();
    });
  }

  public getLatestPublications(): void {
    this.isLoadingPage = true;

    this.repositoryService.getAllPublications()
      .subscribe({
        next: (response: Libro[]) => {
          console.log(response);
          this.publications = response;
          this.isLoadingPage = false;
        },
        error: (error: any) => {
          console.log(error);
          this.isLoadingPage = false;
        }
      });
  }


}
