import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PublicationService } from 'src/app/publication/services/publication.service';
import { Libro } from 'src/app/publication/types/publication.type';

@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styles: [`
  :host ::ng-deep .p-chip.custom-chip{
  background: var(--red-200);
  color: var(--red-700);
  }
  :host ::ng-deep .p-chip.custom-chip-y {
    background: var(--yellow-200);
    color: var(--yellow-700);
  }
  `
  ]
})
export class PublicationComponent {

  private publicationId: number = 0;
  public isLoadingPage: boolean = true;
  public publication!: Libro;

  constructor(
    private publicationService: PublicationService, private route: ActivatedRoute
    ){

      this.route.params.subscribe(params => this.publicationId = params['id']);

  }

  ngOnInit(): void{

    this.getPublicationById();

  }

  private getPublicationById(){

    this.publicationService.getPublicationById(this.publicationId).subscribe({
      next: (response: Libro) => {
        this.publication = response;
        this.isLoadingPage = false;
      },
      error: () => {
        this.isLoadingPage = false;
      }
    });

  }

}
