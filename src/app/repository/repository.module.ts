import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RepositoryRoutingModule } from './repository-routing.module';
import { RepositoryComponent } from './repository.component';
import { PublicationsComponent } from './page/publications.component';
import { PublicationComponent } from './page/publication.component';
import { HomeComponent } from './page/home.component';
import { HeaderComponent } from './components/header.component';
import { FooterComponent } from './components/footer.component';
import { PrimeComponentsModule } from '../prime-components/prime-components.module';
import { SharedModule } from '../shared/shared.module';
import { PublicationModule } from '../publication/publication.module';


@NgModule({
  declarations: [
    RepositoryComponent,
    PublicationsComponent,
    PublicationComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    RepositoryRoutingModule,
    PrimeComponentsModule,
    SharedModule,
    PublicationModule
  ]
})
export class RepositoryModule { }
